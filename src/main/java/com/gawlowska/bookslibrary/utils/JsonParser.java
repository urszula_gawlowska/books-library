package com.gawlowska.bookslibrary.utils;

import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.gawlowska.bookslibrary.model.Book;

public class JsonParser {

	public List<Book> parseJsonToBookObject(String path) {

		JSONParser parser = new JSONParser();
		List<Book> books = new ArrayList<>();

		try {
			Object objectToParse = parser.parse(new FileReader(path));

			JSONObject jsonObject = (JSONObject) objectToParse;
			JSONArray items = getJSONArray(jsonObject, "items");

			Iterator<?> itemsIterator = items.iterator();

			while (itemsIterator.hasNext()) {
				Book book = new Book();
				JSONObject item = (JSONObject) itemsIterator.next();
				JSONObject bookInfo = (JSONObject) item.get("bookInfo");
				JSONArray industryIdentifiersArray = getJSONArray(bookInfo, "industryIdentifiers");
				JSONArray authorsArray = getJSONArray(bookInfo, "authors");
				JSONArray categoriesArray = getJSONArray(bookInfo, "categories");

				setIsbnFromIndustryIdentifiers(industryIdentifiersArray, book);

				if (book.getIsbn() == null)
					book.setIsbn((String) item.get("id"));

				book.setTitle((String) bookInfo.get("title"));

				if (bookInfo.containsKey("subtitle"))
					book.setSubtitle((String) bookInfo.get("subtitle"));

				book.setPublisher((String) bookInfo.get("publisher"));

				setPublishedDateWithUnixFormat(bookInfo, book);

				if (bookInfo.containsKey("description"))
					book.setDescription((String) bookInfo.get("description"));

				if (bookInfo.containsKey("pageCount"))
					book.setPageCount((int) (long) bookInfo.get("pageCount"));

				book.setLanguage((String) bookInfo.get("language"));

				book.setPreviewLink((String) bookInfo.get("previewLink"));

				if (bookInfo.containsKey("averageRating"))
					book.setAverageRating((double) bookInfo.get("averageRating"));

				book.setAuthors(convertArrayToList(authorsArray));

				book.setCategories(convertArrayToList(categoriesArray));

				books.add(book);
			}

		} catch (IOException | org.json.simple.parser.ParseException | ParseException exception) {
			exception.printStackTrace();
		}

		return books;
	}

	private List<String> convertArrayToList(JSONArray array) {
		List<String> arrayAsList = new ArrayList<>();
		Iterator<?> iterator = array.iterator();
		while (iterator.hasNext()) {
			arrayAsList.add((String) iterator.next());
		}
		return arrayAsList;
	}

	private void setPublishedDateWithUnixFormat(JSONObject volumeInfo, Book book) throws ParseException {
		if (volumeInfo.containsKey("publishedDate")) {

			String publishedDateAsString = (String) volumeInfo.get("publishedDate");
			DateFormat dateFormat = new SimpleDateFormat();

			if (publishedDateAsString.length() == 4) {
				dateFormat = new SimpleDateFormat("yyyy");
			} else {
				dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			}

			Date date = dateFormat.parse(publishedDateAsString);
			book.setPublishedDate(date.getTime());
		}
	}

	private void setIsbnFromIndustryIdentifiers(JSONArray industryIdentifiersArray, Book book) {
		Iterator<?> identifiersIterator = industryIdentifiersArray.iterator();
		while (identifiersIterator.hasNext()) {
			JSONObject industryIdentifier = (JSONObject) identifiersIterator.next();
			if (industryIdentifier.containsValue("ISBN_13")) {
				book.setIsbn((String) industryIdentifier.getOrDefault("identifier", null));
			}
		}
	}

	private JSONArray getJSONArray(JSONObject jsonObject, String titleOfProperty) {
		JSONArray jsonArray;
		if (jsonObject.containsKey(titleOfProperty)) {
			jsonArray = (JSONArray) jsonObject.get(titleOfProperty);
		} else {
			jsonArray = new JSONArray();
		}
		return jsonArray;
	}
}
