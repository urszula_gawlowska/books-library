package com.gawlowska.bookslibrary.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.gawlowska.bookslibrary.model.Book;
import com.gawlowska.bookslibrary.utils.JsonParser;

@Repository
public class BooksRepository {

	@Value("${source.data.path}")
	public String getJsonPath;
	
	public List<Book> getAll() {
		
		JsonParser jsonParser = new JsonParser();
		
		List<Book> allBooks = jsonParser.parseJsonToBookObject(getJsonPath);
		
		return allBooks;
	}
}