package com.gawlowska.bookslibrary.model;

import java.util.Comparator;

public class Author {

	private String authorName;

	private double averageRating;

	public Author() {
	}

	public Author(String authorName, double averageRating) {
		this.authorName = authorName;
		this.averageRating = averageRating;
	}

	public static Comparator<Author> authorComparator = (Author author1, Author author2) -> Double
			.compare(author2.getAverageRating(), author1.getAverageRating());

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public double getAverageRating() {
		return averageRating;
	}

	public void setAverageRating(double averageRating) {
		this.averageRating = averageRating;
	}

	/*Added after sending project.*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		long temp;
		temp = Double.doubleToLongBits(averageRating);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (Double.doubleToLongBits(averageRating) != Double.doubleToLongBits(other.averageRating))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Author [authorName=" + authorName + ", averageRating=" + averageRating + "]";
	}
}