package com.gawlowska.bookslibrary.service;

import java.util.List;

import com.gawlowska.bookslibrary.model.Author;
import com.gawlowska.bookslibrary.model.Book;

public interface BookService {
	
	List<Book> getAllBooks();
	
	Book getBookByIsbn(String isbn);
	
	List<Book> getBooksByCategory(String categoryName);
	
	List<Author> getAuthorsByRating();
}
