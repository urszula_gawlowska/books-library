package com.gawlowska.bookslibrary.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gawlowska.bookslibrary.model.Author;
import com.gawlowska.bookslibrary.model.Book;
import com.gawlowska.bookslibrary.repository.BooksRepository;
import com.gawlowska.bookslibrary.service.BookService;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BooksRepository repository;
	
	@Override
	public List<Book> getAllBooks() {
		return repository.getAll();
	}

	@Override
	public Book getBookByIsbn(String isbn) {
		List<Book> fileteredBooks = repository.getAll().stream()
				.filter(bookToFilter -> bookToFilter.getIsbn().equals(isbn)).collect(Collectors.toList());
		if (fileteredBooks.size() == 1) {
			return fileteredBooks.get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<Book> getBooksByCategory(String categoryName) {
		List<Book> booksFromGivenCategory = new ArrayList<>();
		for (Book book : repository.getAll()) {
			if (book.getCategories().contains(categoryName)) {
				booksFromGivenCategory.add(book);
			}
		}
		return booksFromGivenCategory;
	}

	@Override
	public List<Author> getAuthorsByRating() {
		List<Author> authors = new ArrayList<>();

		Iterator iteratorForBooksGroupedByAuthors = getBooksGroupedByAuthors().entrySet().iterator();

		while (iteratorForBooksGroupedByAuthors.hasNext()) {

			Map.Entry pair = (Map.Entry) iteratorForBooksGroupedByAuthors.next();
			List<String> authorsNamesFromOneGroup = (List<String>) pair.getKey();
			List<Book> booksForGivenAuthors = (List<Book>) pair.getValue();

			fillAuthorsList(authors, authorsNamesFromOneGroup, booksForGivenAuthors);
		}

		Collections.sort(authors, Author.authorComparator);

		return authors;
	}

	private void fillAuthorsList(List<Author> authors, List<String> authorsNamesFromOneGroup,
			List<Book> booksForGivenAuthors) {
		for (String authorName : authorsNamesFromOneGroup) {
			Author author = new Author(authorName, getAverageRatingForAuthor(booksForGivenAuthors));
			authors.add(author);
		}
	}

	private Map<List<String>, List<Book>> getBooksGroupedByAuthors() {
		Map<List<String>, List<Book>> booksGroupedByAuthors = getBooksWithPresentAuthorsAndRating().stream()
				.collect(Collectors.groupingBy(Book::getAuthors, Collectors.toList()));
		return booksGroupedByAuthors;
	}

	private List<Book> getBooksWithPresentAuthorsAndRating() {
		List<Book> booksWithAuthorsAndRating = new ArrayList<>();
		for (Book book : repository.getAll()) {
			if (book.getAuthors() != null && book.getAverageRating() != 0.0)
				booksWithAuthorsAndRating.add(book);
		}
		return booksWithAuthorsAndRating;
	}

	private double getAverageRatingForAuthor(List<Book> booksForGivenAuthors) {
		List<Double> ratingsForAuthor = new ArrayList<>();
		double sumOfRatings = 0.0;

		for (Book book : booksForGivenAuthors) {
			ratingsForAuthor.add(book.getAverageRating());
			sumOfRatings += book.getAverageRating();
		}

		double averageRatingForAuthor = sumOfRatings / ratingsForAuthor.size();

		return averageRatingForAuthor;
	}
}
