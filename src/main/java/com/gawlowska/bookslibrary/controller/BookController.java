package com.gawlowska.bookslibrary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gawlowska.bookslibrary.model.Author;
import com.gawlowska.bookslibrary.model.Book;
import com.gawlowska.bookslibrary.service.BookService;

@RestController
@RequestMapping(value = "/api")
public class BookController {
	
	@Autowired
	private BookService service;
	
	@GetMapping(value = "/books")
	public ResponseEntity<List<Book>> getAllBooks() {
		return ResponseEntity.ok(service.getAllBooks());
	}
	
	@GetMapping(value = "/book/{isbn}")
	public ResponseEntity<?> getByIsbn(@PathVariable(value="isbn", required=true) String isbn) {
		if (service.getBookByIsbn(isbn) == null)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("404 \"No results found\"");
		return ResponseEntity.ok(service.getBookByIsbn(isbn));
	}
	
	@GetMapping(value = "/category/{categoryName}/books")
	public ResponseEntity<List<Book>> getByCategory(@PathVariable(value="categoryName", required=true) String categoryName) {
		return ResponseEntity.ok(service.getBooksByCategory(categoryName));
	}
	
	@GetMapping(value = "/rating")
	public ResponseEntity<List<Author>> getAuthorRating() {
		return ResponseEntity.ok(service.getAuthorsByRating());
	}
}
