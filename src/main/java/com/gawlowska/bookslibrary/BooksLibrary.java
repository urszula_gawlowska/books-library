package com.gawlowska.bookslibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BooksLibrary {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(BooksLibrary.class, args);
	}
}
