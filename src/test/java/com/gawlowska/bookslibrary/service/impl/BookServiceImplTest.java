package com.gawlowska.bookslibrary.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.gawlowska.bookslibrary.model.Author;
import com.gawlowska.bookslibrary.model.Book;
import com.gawlowska.bookslibrary.repository.BooksRepository;

public class BookServiceImplTest {

	@InjectMocks
	private BookServiceImpl bookService;

	@Mock
	private BooksRepository repository;

	private List<Book> books;
	private Book javaBook;
	private Book androidBook;

	@Before
	public void prepare() {

		MockitoAnnotations.initMocks(this);

		books = new ArrayList<>();

		javaBook = new Book();
		javaBook.setIsbn("j1");
		javaBook.setTitle("Java book");
		javaBook.setCategories(Arrays.stream("science,IT".split(",")).collect(Collectors.toList()));
		javaBook.setAuthors(Arrays.stream("Amy Doe".split(",")).collect(Collectors.toList()));
		javaBook.setAverageRating(4.5);
		books.add(javaBook);

		androidBook = new Book();
		androidBook.setIsbn("a1");
		androidBook.setTitle("Android book");
		androidBook.setCategories(Arrays.stream("IT".split(",")).collect(Collectors.toList()));
		androidBook.setAuthors(Arrays.stream("Ben James,John Snow".split(",")).collect(Collectors.toList()));
		androidBook.setAverageRating(3.2);
		books.add(androidBook);

		when(repository.getAll()).thenReturn(books);
	}

	@Test
	public void getBookByIsbnTest() {
		assertEquals(javaBook, bookService.getBookByIsbn("j1"));
	}

	@Test
	public void getBooksByCategoryTest() {
		assertEquals(Arrays.asList(javaBook), bookService.getBooksByCategory("science"));
		assertEquals(Arrays.asList(javaBook, androidBook), bookService.getBooksByCategory("IT"));
	}

	@Test
	public void getAuthorsByRatingTest() {
		Author amyDoeAuthor = new Author("Amy Doe", 4.5);
		Author benJamesAuthor = new Author("Ben James", 3.2);
		Author johnSnowAuthor = new Author("John Snow", 3.2);
		List<Author> authors = Arrays.asList(amyDoeAuthor, benJamesAuthor, johnSnowAuthor);

		assertEquals(authors, bookService.getAuthorsByRating());
	}
}
