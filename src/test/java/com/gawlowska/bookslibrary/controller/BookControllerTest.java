package com.gawlowska.bookslibrary.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.gawlowska.bookslibrary.model.Author;
import com.gawlowska.bookslibrary.model.Book;
import com.gawlowska.bookslibrary.service.BookService;
import com.gawlowska.bookslibrary.controller.BookController;

@RunWith(SpringRunner.class)
@WebMvcTest(value = BookController.class)
public class BookControllerTest {

	@LocalServerPort
	private String port;

	@MockBean
	private BookService service;

	@Autowired
	private MockMvc mockMvc;

	private static final String URI = "/api";

	@Test
	public void getByIsbnTest() throws Exception {
		// given
		String isbn = "100";
		Book book = new Book();
		book.setIsbn(isbn);
		when(service.getBookByIsbn(isbn)).thenReturn(book);
		final String getByIsbn = URI + "/book/" + isbn;
		// when
		mockMvc.perform(get(getByIsbn))
				// then
				.andExpect(status().isOk());
	}

	@Test
	public void getByIsbnNotFoundTest() throws Exception {
		// given
		String isbn = "1";
		when(service.getBookByIsbn(isbn)).thenReturn(null);
		final String getByIsbn = URI + "/book/" + isbn;
		// when
		mockMvc.perform(get(getByIsbn))
				// then
				.andExpect(status().isNotFound()).andExpect(content().string("404 \"No results found\""));
	}

	@Test
	public void getByCategoryTest() throws Exception {
		// given
		String categoryName = "Computers";
		List<String> categories = new ArrayList<>();
		categories.add(categoryName);

		Book book = new Book();
		book.setCategories(categories);

		List<Book> booksFilteredByCategory = new ArrayList<>();
		booksFilteredByCategory.add(book);

		when(service.getBooksByCategory(categoryName)).thenReturn(booksFilteredByCategory);
		final String getByCategory = URI + "/category/" + categoryName + "/books";
		// when
		mockMvc.perform(get(getByCategory))
				// then
				.andExpect(status().isOk());
	}

	@Test
	public void getByCategoryReturnEmptyListTest() throws Exception {
		// given
		String categoryName = "Computers";
		String emptyListAsString = new ArrayList<>().toString();
		final String getByCategory = URI + "/category/" + categoryName + "/books";
		// when
		mockMvc.perform(get(getByCategory))
				// then
				.andExpect(status().isOk()).andExpect(content().string(emptyListAsString));
	}

	@Test
	public void getAuthorRating() throws Exception {
		// given
		Author author = new Author();
		List<Author> authors = new ArrayList<>();
		authors.add(author);

		when(service.getAuthorsByRating()).thenReturn(authors);
		final String getAuthorsRating = URI + "/rating";
		// when
		mockMvc.perform(get(getAuthorsRating))
				// then
				.andExpect(status().isOk());
	}
}
